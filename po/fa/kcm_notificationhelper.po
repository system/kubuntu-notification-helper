# Persian translation for kubuntu-notification-helper
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the kubuntu-notification-helper package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: kubuntu-notification-helper\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:39+0000\n"
"PO-Revision-Date: 2010-07-03 22:16+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Persian <fa@li.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-04-01 12:20+0000\n"
"X-Generator: Launchpad (build 17413)\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: src/kcmodule/notificationhelperconfigmodule.cpp:53
#, kde-format
msgid "Kubuntu Notification Helper Configuration"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:57
#, kde-format
msgid "(C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:59
#, kde-format
msgid "Jonathan Thomas"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:60
#, kde-format
msgid "Harald Sitter"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:65
#, kde-format
msgid "Configure the behavior of Kubuntu Notification Helper"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:72
#, kde-format
msgid "Show notifications for:"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:74
#, kde-format
msgid "Application crashes"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:75
#, kde-format
msgid "Proprietary Driver availability"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:76
#, kde-format
msgid "Upgrade information"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:77
#, kde-format
msgid "Restricted codec availability"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:78
#, kde-format
msgid "Incomplete language support"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:79
#, kde-format
msgid "Required reboots"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:91
#, kde-format
msgid "Notification type:"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:95
#, kde-format
msgid "Use both popups and tray icons"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:97
#, kde-format
msgid "Tray icons only"
msgstr ""

#: src/kcmodule/notificationhelperconfigmodule.cpp:99
#, kde-format
msgid "Popup notifications only"
msgstr ""
